﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Shapes.Rectangles
{
    class Square : Rectangle
    {
        private byte _width;
        public override byte Width
        {
            get => _width;
            set
            {
                _width = value;
                _height = value;
            }
        }

        private byte _height;
        public override byte Height
        {
            get => _height;
            set
            {
                _height = value;
                _width = value;
            }
        }
    }
}
