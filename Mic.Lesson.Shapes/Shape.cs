﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Shapes
{
    abstract class Shape
    {
        public char symbol;
        public ConsoleColor symbolColor;
        public ConsoleColor fonColor;

        protected Shape()
        {
            Width = 10;
            symbol = '*';
            symbolColor = Console.ForegroundColor;
            fonColor = Console.BackgroundColor;
        }

        public virtual byte Width { get; set; }
        public virtual byte Height { get; set; }

        public abstract void Draw();
        //public virtual void Draw() { }
    }
}