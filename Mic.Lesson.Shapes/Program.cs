﻿using Mic.Lesson.Shapes.Lines;
using Mic.Lesson.Shapes.Rectangles;
using Mic.Lesson.Shapes.Triangles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Shapes
{
    class Program
    {
        static void Main(string[] args)
        {
            var shape = new Arrow { symbol = '*' };
            shape.Height = 10;
            
            shape.Draw();

            //Print(line);

            //var arrow = new Arrow { symbol = '-' };
            //Print(arrow);

            //var darrow = new DoubleArrow { symbol = '-' };
            //Print(darrow);

            Console.ReadLine();
        }

        static void Print(Line line)
        {
            string typeName = line.GetType().Name;
            Console.WriteLine($"********* {typeName} *********");

            line.Draw();

            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
