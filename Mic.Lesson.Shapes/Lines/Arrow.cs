﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Shapes.Lines
{
    class Arrow : Line
    {
        public override void Draw()
        {
            base.Draw();

            if (IsVertical)
                Console.Write(">");
            else
                Console.WriteLine("V");
        }
    }
}
