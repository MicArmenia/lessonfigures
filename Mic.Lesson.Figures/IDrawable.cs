﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figures
{
    interface IDrawable
    {
        void Draw();
        ConsoleColor Color { get; set; }
    }
}