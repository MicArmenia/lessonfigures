﻿using Mic.Lesson.Figures.Shapes.Lines;
using Mic.Lesson.Figures.Shapes.Rectangles;
using Mic.Lesson.Figures.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figures
{
    class Program
    {
        static void Main(string[] args)
        {
            var shape = new Square { symbol = '*' };
            shape.Height = 10;
            Print(shape);

            Console.WriteLine();

            var table = new Table { Color = ConsoleColor.Red };
            //table.Draw();
            Print(table);

            //shape.Draw();

            //Print(line);

            //var arrow = new Arrow { symbol = '-' };
            //Print(arrow);

            //var darrow = new DoubleArrow { symbol = '-' };
            //Print(darrow);

            Console.ReadLine();
        }

        static void Print(IDrawable figure)
        {
            string typeName = figure.GetType().Name;
            Console.WriteLine($"********* {typeName} *********");

            Console.ForegroundColor = figure.Color;
            figure.Draw();
            Console.ResetColor();

            Console.WriteLine();
            Console.WriteLine();
        }

        //static void Print(Shape figure)
        //{
        //    string typeName = figure.GetType().Name;
        //    Console.WriteLine($"********* {typeName} *********");

        //    figure.Draw();

        //    Console.WriteLine();
        //    Console.WriteLine();
        //}

        //static void Print(Table figure)
        //{
        //    string typeName = figure.GetType().Name;
        //    Console.WriteLine($"********* {typeName} *********");

        //    figure.Draw();

        //    Console.WriteLine();
        //    Console.WriteLine();
        //}
    }
}
