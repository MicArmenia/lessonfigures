﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figures
{
    abstract class Shape : IDrawable
    {
        public char symbol;

        protected Shape()
        {
            Width = 10;
            symbol = '*';
            Color = Console.ForegroundColor;
        }

        public ConsoleColor Color { get; set; }
        public virtual byte Width { get; set; }
        public virtual byte Height { get; set; }

        public abstract void Draw();
        //public virtual void Draw() { }
    }
}