﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figures.Shapes.Lines
{
    class Line : Shape
    {
        private byte _width;
        public sealed override byte Width
        {
            get => _width;
            set
            {
                _width = value;
                _height = 0;
            }
        }

        private byte _height;
        public sealed override byte Height
        {
            get => _height;
            set
            {
                _height = value;
                _width = 0;
            }
        }

        public bool IsVertical => Width > 0;

        public override void Draw()
        {
            if (IsVertical)
                Console.Write(new string(symbol, Width));
            else
            {
                for (int i = 0; i < Height; i++)
                {
                    Console.WriteLine(symbol);
                }
            }
        }
    }
}
