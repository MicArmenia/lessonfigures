﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.Figures.Tables
{
    class Table : IDrawable
    {
        public Table()
        {
            row = 10;
            column = 10;
            Color = ConsoleColor.White;
        }

        public int row;
        public int column;

        public ConsoleColor Color { get; set; }

        public void Draw()
        {
            for (int i = 1; i <= row; i++)
            {
                for (int j = 1; j <= column; j++)
                {
                    Console.Write("------|");
                }
                Console.WriteLine();
            }
        }
    }
}