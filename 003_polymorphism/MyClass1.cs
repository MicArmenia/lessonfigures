﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003_polymorphism
{
    class MyClass1
    {
        public MyClass1()
        {
            Print();
        }

        //0x001
        public virtual void Print()
        {
            Console.WriteLine("MyClass1");
        }
    }
}
