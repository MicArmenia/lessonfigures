﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003_polymorphism
{
    class MyClass2 : MyClass1
    {
        public MyClass2() : base()
        {
            Print();
        }

        //0x005
        public new void Print()
        {
            Console.WriteLine("MyClass2");
        }
    }
}
