﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_polymorphism
{
    class Student
    {
        public string name;
        public string surname;

        public string Fullname => $"{surname} {name}";

        public override string ToString()
        {
            return Fullname;
        }
    }
}
