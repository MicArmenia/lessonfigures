﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            var st = new Student { name = "A1", surname = "A1yan" };

            Console.WriteLine(st);

            List<Student> students = CreateStudents(15);

            Console.ReadLine();
        }

        static List<Student> CreateStudents(int count)
        {
            var list = new List<Student>(count);
            for (int i = 0; i < count; i++)
            {
                var st = new Student { name = $"A{i+1}", surname = $"A{i + 1}yan" };
                list.Add(st);
            }
            return list;
        }
    }
}
